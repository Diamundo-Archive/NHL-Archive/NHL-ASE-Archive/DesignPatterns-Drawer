package nl.kevinnauta.designpatternsdrawer;

import java.awt.Rectangle;

/**
 * Interface for the items that can be drawn.
 */
public interface DrawableItem {
	
	public DrawableItem setID(int newID);
	public int getID();
	
	public String toString(int tabs);
	public Rectangle getSize();

	public default String tabs(int tabs) {
    	String tmp = "";
    	while(tabs > 0) {
    		tmp += "\t";
    		tabs--;
    	}
    	return tmp;
    }
	
}
