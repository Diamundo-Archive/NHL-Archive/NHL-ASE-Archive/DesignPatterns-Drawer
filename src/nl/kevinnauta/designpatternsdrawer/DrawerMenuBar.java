package nl.kevinnauta.designpatternsdrawer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

@SuppressWarnings("serial")
public class DrawerMenuBar extends JMenuBar {
    private final DrawerFrame listener;
    public DrawerFrame getListener() { return listener; }

    /**
     * The respective Menus and Menu Items.
     */
    private JMenu menuFile;
    private JMenuItem menuFileNew;
    private JMenuItem menuFileOpen;
    private JMenuItem menuFileSave;
    private JMenuItem menuFileSaveAs;
    private JMenuItem menuFileExit;

    private JMenu menuEdit;
    private JMenuItem menuEditNewItem;
    private JMenuItem menuEditEditItem;
    private JMenuItem menuEditRemoveItem;
    private JMenuItem menuEditUndo;
    private JMenuItem menuEditRedo;

    private JMenu menuView;
    private JMenuItem menuViewRefresh;

    private JMenu menuAbout;
    private JMenuItem menuAboutAbout;
    private JMenuItem menuAboutShortcuts;

    /**
     * DrawerMenuBar constructor: initializing the MenuBar, setting titles and actionlisteners.
     * @param listener Pointer to the DrawerFrame, where the actionlisteners point to.
     */
	public DrawerMenuBar(final DrawerFrame listener){
        this.listener = listener;

        menuFile = new JMenu("File");
        menuFileNew = new JMenuItem("New");
        menuFileOpen = new JMenuItem("Open...");
        menuFileSave = new JMenuItem("Save");
        menuFileSaveAs = new JMenuItem("Save As...");
        menuFileExit = new JMenuItem("Exit");

        menuEdit = new JMenu("Edit");
        menuEditNewItem = new JMenuItem("Add a new item...");
        menuEditEditItem = new JMenuItem("Edit this item...");
        menuEditRemoveItem = new JMenuItem("Remove this item...");
        menuEditUndo = new JMenuItem("Undo last action");
        menuEditRedo = new JMenuItem("Redo last undone action");

        menuView = new JMenu("View");
        menuViewRefresh = new JMenuItem("Refresh");

        menuAbout = new JMenu("About");
        menuAboutAbout = new JMenuItem("About");
        menuAboutShortcuts = new JMenuItem("Shortcuts");
        
        
        /**
         * All ActionListeners pointing to their function in DrawerFrame
         * @see DrawerFrame
         */
        menuFileNew.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuFileNew(); }
        });

        menuFileOpen.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuFileOpen(); }
        });

        menuFileSave.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuFileSave(); }
        });

        menuFileSaveAs.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuFileSaveAs(); }
        });
        
        menuFileExit.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuFileExit(); }
        });
        
        menuEditNewItem.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuEditNewItem(); }
        });

        menuEditEditItem.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuEditEditItem( listener.getModel().getSelectedIDs() ); }
        });

        menuEditRemoveItem.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuEditRemoveItem( listener.getModel().getSelectedIDs() ); }
        });
        
        menuEditUndo.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuEditUndo(); }
        });

        menuEditRedo.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuEditRedo(); }
        });
        
        menuViewRefresh.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuViewRefresh(); }
        });

        menuAboutAbout.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuAboutAbout(); }
        });
        
        menuAboutShortcuts.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent arg0) { listener.menuAboutShortcuts(); }
        });    
        
        /**
         * The MenuBar Structure.
         */
        menuFile.add(menuFileNew);
        menuFile.add(menuFileOpen);
        menuFile.add(menuFileSave);
        menuFile.add(menuFileSaveAs);
        menuFile.add(menuFileExit);
        this.add(menuFile);

        menuEdit.add(menuEditNewItem);
        menuEdit.add(menuEditEditItem);
        menuEdit.add(menuEditRemoveItem);
        menuEdit.add(menuEditUndo);
        menuEdit.add(menuEditRedo);
        this.add(menuEdit);
        
        menuView.add(menuViewRefresh);
        this.add(menuView);

        menuAbout.add(menuAboutAbout);
        menuAbout.add(menuAboutShortcuts);
        this.add(menuAbout);
    }
	
	/**
	 * Function that enables or disables certain MenuBar items based on the model's state.
	 * @param drawerModel The new state of the model.
	 */
	public void onNotify(DrawerModel drawerModel) {
		menuFileSave.setEnabled( drawerModel.isEdited() );

		menuEditEditItem.setEnabled( ! drawerModel.getSelectedIDs().contains(0) );
		menuEditRemoveItem.setEnabled(! drawerModel.getSelectedIDs().contains(0) );
		
		menuEditUndo.setEnabled( listener.commandStack.canUndo() );
		menuEditRedo.setEnabled( listener.commandStack.canRedo() );
		
	}

}
