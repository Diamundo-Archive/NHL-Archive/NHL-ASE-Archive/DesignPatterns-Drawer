package nl.kevinnauta.designpatternsdrawer;

public abstract class Figure implements DrawableItem {
	
	private int iD;
	public Figure setID( int newID ) { this.iD = newID; return this; }
	public int getID() { return this.iD; }
	
}
