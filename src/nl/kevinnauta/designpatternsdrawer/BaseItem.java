package nl.kevinnauta.designpatternsdrawer;

import java.awt.*;

public class BaseItem extends Figure implements DrawableItem {
	private int left, top, width, height;

	public enum TYPE { //type is in caps, so it doesn't interfere with the variable Type
		RECTANGLE, ELLIPSE
	}
	private TYPE type;
	public void setType(TYPE t)  { this.type = t; }
	public void setType(String t){
		switch(t.toLowerCase()){
			case "rectangle" :
				this.type = TYPE.RECTANGLE; break;
			case "ellipse":
				this.type = TYPE.ELLIPSE; break;
			default:
				System.err.println("WRONG TYPE: " + t);
		}
	}
	public TYPE getType() { return this.type; }

	public int  getLeft() 		{ return left;  }
	public int  getTop() 		{ return top; 	}
	public int  getWidth() 		{ return width; }
	public int  getHeight() 	{ return height;}

	public void setLeft(int left) 		{ this.left = left; 	}
	public void setTop(int top) 		{ this.top = top; 		}
	public void setWidth(int width) 	{ this.width = width; 	}
	public void setHeight(int height) 	{ this.height = height; }
	
    public BaseItem(){ /* empty constructor */ }
    
    public BaseItem(String args){
    	this();
    	String[] split = args.split(" ");
    	this.setType  ( split[0] );
    	this.setLeft  ( Integer.parseInt(split[1]) );
    	this.setTop   ( Integer.parseInt(split[2]) );
    	this.setWidth ( Integer.parseInt(split[3]) );
    	this.setHeight( Integer.parseInt(split[4]) );
    }

	public BaseItem(TYPE newType, int newLeft, int newTop, int newWidth, int newHeight){
		this();
		this.setType  ( newType );
		this.setLeft  ( newLeft );
		this.setTop   ( newTop );
		this.setWidth ( newWidth );
		this.setHeight( newHeight );
	}

	public BaseItem(String newType, int newLeft, int newTop, int newWidth, int newHeight){
		this();
		this.setType  ( newType );
		this.setLeft  ( newLeft );
		this.setTop   ( newTop );
		this.setWidth ( newWidth );
		this.setHeight( newHeight );
	}

	@Override
    public String toString() {
		return this.toString(0);
    }

	@Override
	public String toString(int tabs) {
		return tabs(tabs) + this.getType().toString().toLowerCase() + " " + (DEBUG.ID ? "(" + this.getID() + ") " : "")  + this.getLeft() + " " + this.getTop() + " " + this.getWidth() + " " + this.getHeight();
	}

	@Override
    public Rectangle getSize() {
    	return new Rectangle(getLeft(), getTop(), getWidth(), getHeight());
    }
	
}
